<?php

namespace App\Http\Controllers;

use App\Contracts\ProductContract;
use App\Contracts\ProductFormRequestContract;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductResource;
use App\PaymentHistory;
use App\Services\OrderService;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;

class OrderProductRequestController extends Controller
{
    public function store(ProductFormRequestContract $request, ProductContract $product)
    {
        if((new AuthAbilityService())->userHasAbility('billing-order-create') && $request->has('user_id')){
            $userId = $request->user_id;
        } else {
            $userId = (new AuthUserService())->getId();
        }

        $product->fillData($request->validated());
        $product = $product->storeProduct();

        $orderService = new OrderService();
        $order = $orderService->create([
            'user_id' =>  $userId
        ], [$product->id]);
        return new OrderResource($order);
    }
}
