<?php

namespace App\Http\Controllers;

use App\Contracts\ProductContract;
use App\Contracts\ProductFormRequestContract;
use App\Http\Resources\ProductResource;
use App\Product;

class ProductController extends Controller
{
    public function store(ProductFormRequestContract $request, ProductContract $product)
    {
        $product->fillData($request->validated());
        return new ProductResource($product->storeProduct());
    }
}
