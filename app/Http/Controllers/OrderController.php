<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrder;
use App\Http\Requests\UpdateOrder;
use App\Http\Resources\OrderResource;
use App\Services\OrderService;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;
use Illuminate\Support\Facades\Input;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;
use App\Order;


/**
 * @group OrderController
 *
 * APIs для работа с заказами
 */

class OrderController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Order::class);
    }


    /**
     * List Orders
     *
     * @response{
     *   "data": [
     *   {
     *      "id": 1,
     *      "created_at": "2018-10-24 09:22:37",
     *      "updated_at": "2018-10-25 08:59:59"
     *   }
     * ]
     *
     */

    public function index()
    {
        $orders = QueryBuilder::for (Order::class)
            ->allowedIncludes(['products', 'statuses', 'histories'])
            ->allowedFilters(
                Filter::exact('id'),
                Filter::exact('user_id')
            );

        if(!(new AuthAbilityService())->userHasAbility('billing-order-view')) {
            $orders->where('user_id', (new AuthUserService)->getId());
        }

        return OrderResource::collection($orders->jsonPaginate());
    }

    /**
     * Create Order
     *
     * @queryParam user_id
     * @queryParam title
     * @queryParam description
     *
     * @bodyParam user_id integer required 7
     * @bodyParam title string  Title
     * @bodyParam description string Description
     *
     * @response{
     *   "data": {
     *      "id": 7,
     *      "title": "Title",
     *      "description": "Description",
     *      "created_at": "2018-10-29 08:42:02",
     *      "updated_at": "2018-10-29 08:42:02"
     *   }
     * }
     */

    public function store(StoreOrder $request)
    {
        $orderService = new OrderService();
        return new OrderResource($orderService->create($request->validated()));
    }

    /**
     * Get info Orer
     *
     * @queryParam id required индификатор
     *
     * @response{
     *   "data": {
     *      "id": 1,
     *      "title": "Title",
     *      "description": "Description",
     *      "created_at": "2018-10-24 09:22:37",
     *      "updated_at": "2018-10-25 08:59:59"
     *   }
     * }
     *
     */

    public function show(Order $order)
    {
        $order = QueryBuilder::for (Order::class)
            ->allowedIncludes(['products', 'statuses', 'histories'])
            ->where('id', $order->id)->first();

        return new OrderResource($order);
    }

    /**
     * Update Order
     *
     * @queryParam id required
     * @queryParam title
     * @queryParam description
     *
     * @bodyParam id integer required 7
     * @bodyParam title string  Title
     * @bodyParam description string Description
     *
     * @response{
     *   "data": {
     *      "id": 7,
     *      "title": "Title",
     *      "description": "Description",
     *      "created_at": "2018-10-29 08:42:02",
     *      "updated_at": "2018-10-29 08:42:02"
     *   }
     * }
     */

    public function update(UpdateOrder $request, Order $order)
    {
        $order->update($request->validated());

        return new OrderResource($order);
    }

}