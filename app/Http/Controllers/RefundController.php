<?php

namespace App\Http\Controllers;

use App\Contracts\ProductContract;
use App\Http\Requests\RefundOrder;
use App\Http\Resources\OrderResource;
use App\Order;
use App\Statuses\Order\OrderRefundStatus;

class RefundController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Order::class);
    }

    public function update(RefundOrder $request, Order $order)
    {
        foreach ($order->products as $product)
        {
            /* @var $productService ProductContract */
            $productService = app()->makeWith(ProductContract::class, [$product]);
            $productService->refund();
        }

        $order->setStatus(new OrderRefundStatus());
        return new OrderResource($order);
    }
}
