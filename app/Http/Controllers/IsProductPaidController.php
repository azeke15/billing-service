<?php

namespace App\Http\Controllers;

use App\Http\Requests\IsProductPaid;
use App\Order;
use App\Product;

class IsProductPaidController extends Controller
{
    public function show(IsProductPaid $request, $type, $product_id) {
        $product = Product::where('type', $type)->where('data->document_id', (int) $product_id)->first();
        $order = Order::where('id', $product->order_id)->first();
        if ($order->success_payment_id == null) {
            return response()->json([
                'code'     => 402,
                'order_id' => $order->id
            ], 200);
        } else {
            return response()->json([
                'code'     => 200,
                'order_id' => $order->id
            ], 200);
        }
    }
}
