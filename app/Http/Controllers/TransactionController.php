<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\Contracts\AccountTransactionableContract;
use App\Http\Requests\StoreTransaction;
use App\Http\Resources\TransactionResource;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;
use Illuminate\Support\Facades\Input;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;

class TransactionController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(AccountTransaction::class);
    }

    public function index()
    {
        $transactions = QueryBuilder::for(AccountTransaction::class)
            ->allowedIncludes('account')
            ->allowedFilters(
                Filter::exact('id'),
                Filter::exact('account_id'),
                Filter::exact('type')
            );

        if(!(new AuthAbilityService())->userHasAbility('billing-account-transaction-view')) {
            $transactions->whereHas('account', function ($query) {
                $query->where('user_id', (new AuthUserService)->getId());
            });
        }

        return TransactionResource::collection($transactions->jsonPaginate());
    }

    public function store(StoreTransaction $request, AccountTransactionableContract $account)
    {
        $transaction = new AccountTransaction($request->validated());
        $transaction->account()->associate($account);
        $transaction->save();
        $account->applyTransaction($transaction);
        return new TransactionResource($transaction);
    }

    public function show(AccountTransaction $transaction)
    {
        $transaction = QueryBuilder::for(AccountTransaction::class)
            ->allowedIncludes('account')
            ->where('id', $transaction->id)->first();

        return new TransactionResource($transaction);
    }
}
