<?php

namespace App\Http\Controllers;

use App\Contracts\PaymentContract;
use App\Contracts\PaymentFormRequestContract;
use App\PaymentHistory;

class PaymentController extends Controller
{

    public function store(PaymentFormRequestContract $request, PaymentContract $payment)
    {
        $payment->fillData($request);
        $payment->checkAmount();
        return $payment->process();
    }
}
