<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Requests\StoreAccount;
use App\Http\Requests\UpdateAccount;
use App\Http\Resources\AccountResource;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;

class AccountController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Account::class);
    }

    public function index()
    {
        $accounts = Account::withoutGlobalScope('type');

        if(!(new AuthAbilityService())->userHasAbility('billing-account-view')) {
            $accounts->where('user_id', (new AuthUserService)->getId());
        }

        return AccountResource::collection($accounts->jsonPaginate());
    }

    public function show(Account $account)
    {
        return new AccountResource($account);
    }

    public function store(StoreAccount $request)
    {
        return new AccountResource(Account::create($request->validated()));
    }

    public function update(UpdateAccount $request, Account $account)
    {
        $account->update($request->validated());

        return new AccountResource($account);
    }
}
