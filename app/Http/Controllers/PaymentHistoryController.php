<?php

namespace App\Http\Controllers;

use App\Filters\FiltersUserId;
use App\Http\Resources\PaymentHistoryResource;
use App\PaymentHistory;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;
use Illuminate\Support\Facades\Input;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;

/**
 * @group PaymentHistoryController
 *
 * APIs для работа с Истории платежей
 */

class PaymentHistoryController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(PaymentHistory::class);
    }

    /**
     * List PaymentHistory
     *
     * @response{
     *   "data": [
     *   {
     *      "id": 1,
     *      "created_at": "2018-10-24 09:22:37",
     *      "updated_at": "2018-10-25 08:59:59"
     *   }
     * ]
     *
     */

    public function index()
    {
        $payments = QueryBuilder::for(PaymentHistory::class)
            ->allowedIncludes(['order','statuses'])
            ->allowedFilters(
                Filter::exact('id'),
                Filter::exact('order_id'),
                Filter::custom('user_id',FiltersUserId::class)
            );

        if(!(new AuthAbilityService())->userHasAbility('billing-payment-history-view')) {
            $payments->whereHas('order', function ($query) {
                $query->where('user_id', (new AuthUserService)->getId());
            });
        }

        return PaymentHistoryResource::collection($payments->jsonPaginate());
    }

    public function show(PaymentHistory $paymentHistory)
    {
        $payment = QueryBuilder::for(PaymentHistory::class)
            ->allowedIncludes(['order','statuses'])
            ->where('id', $paymentHistory->id)->first();

        return new PaymentHistoryResource($payment);
    }
}
