<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProductsInOrder;
use App\Http\Resources\OrderResource;
use App\Order;
use App\Product;
use App\Services\OrderService;

class OrderProductsController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Order::class);
    }

    public function update(UpdateProductsInOrder $request, Order $order)
    {
        $orderService = new OrderService();
        $orderService->associateProducts($order, $request->validated()['products']);
        return new OrderResource($order);
    }
}
