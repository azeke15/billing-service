<?php

namespace App\Http\Requests\Payment;

use App\Contracts\PaymentFormRequestContract;
use App\Order;
use App\Statuses\Order\OrderPaidStatus;
use Illuminate\Foundation\Http\FormRequest;

class StorePaymentQiwi extends FormRequest implements PaymentFormRequestContract
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoice' => 'required|exists:orders,id',
            'sum'     => 'required|integer',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            //Проверка статуса продукта
            $order = Order::findOrFail(request()->invoice);
            if ($order->statuses()->where('status_id', '>=', (new OrderPaidStatus())->getStatus())->exists()) {
                $validator->errors()->add('order', trans('validation.orders.check-status'));
            }
        });
    }
}
