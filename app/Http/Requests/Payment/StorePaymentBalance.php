<?php

namespace App\Http\Requests\Payment;

use App\Accounts\Balance;
use App\Order;
use App\OrderStatus;
use App\Product;
use App\Contracts\PaymentFormRequestContract;
use App\Services\Product\ProductBalanceService;
use App\Statuses\Order\OrderPaidStatus;
use Illuminate\Foundation\Http\FormRequest;

class StorePaymentBalance extends FormRequest implements PaymentFormRequestContract
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            //Проверка типа продукта
            $order = Order::findOrFail($this->request->get('order_id'));
            foreach ($order->products as $product) {
                if ($product->type === ProductBalanceService::type) {
                    $validator->errors()->add('order', trans('validation.orders.balance-with-balance'));
                }
            }
            //Проверка баланса
            $balance = Balance::forUser($order->user_id);
            if ($balance->balance < $order->cost) {
                $validator->errors()->add('order', trans('validation.orders.check-balance'));
            }
            //Проверка статуса продукта
            if ($order->statuses()->where('status_id', '>=', (new OrderPaidStatus())->getStatus())->exists()) {
                $validator->errors()->add('order', trans('validation.orders.check-status'));
            }
        });
    }
}
