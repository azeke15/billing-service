<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOrder extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'        => [
                'required',
                function ($attribute, $value, $fail) {
                    $class = config('billing.accounts_namespace').studly_case($value);
                    if (! class_exists($class)) {
                        $fail($attribute.' is invalid.');
                    }
                },
                Rule::unique('accounts', 'type')->where(function ($query) {
                    $query->where('user_id', $this->request->get('user_id'));
                }),
            ],
            'user_id'     => [
                'required',
                'integer',
            ],
            'title'       => 'string',
            'description' => 'string',
        ];
    }
}
