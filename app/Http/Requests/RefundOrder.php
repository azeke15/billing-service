<?php

namespace App\Http\Requests;

use App\Product;
use App\Statuses\Order\OrderPaidStatus;
use App\Statuses\Order\OrderRefundStatus;
use Illuminate\Foundation\Http\FormRequest;

class RefundOrder extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $order->user_id == (new AuthUserService())->getId() or (new AuthAbilityService())->userHasAbility('billing-order-view');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>''
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $order = $this->route('order');

            if($order->statuses()->orderBy('id','desc')->first()->status_id < (new OrderPaidStatus())->getStatus())
            {
                $validator->errors()->add('order', trans('validation.orders.order-not-paid'));
            }

            if($order->statuses()->orderBy('id','desc')->first()->status_id >= (new OrderRefundStatus())->getStatus())
            {
                $validator->errors()->add('order', trans('validation.orders.order-refunded'));
            }

        });
    }
}
