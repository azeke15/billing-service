<?php

namespace App\Http\Requests\Product;

use App\Contracts\PaymentFormRequestContract;
use App\Contracts\ProductFormRequestContract;
use App\Order;
use App\Statuses\Order\OrderPaidStatus;
use Illuminate\Foundation\Http\FormRequest;

class StoreProductBalance extends FormRequest implements ProductFormRequestContract
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sum'   => 'required|integer',
        ];
    }
}
