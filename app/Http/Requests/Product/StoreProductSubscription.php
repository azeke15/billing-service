<?php

namespace App\Http\Requests\Product;

use App\Contracts\PaymentFormRequestContract;
use App\Contracts\ProductFormRequestContract;
use App\Order;
use App\Statuses\Order\OrderPaidStatus;
use GuzzleHttp\Client;
use Illuminate\Foundation\Http\FormRequest;

class StoreProductSubscription extends FormRequest implements ProductFormRequestContract
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_id' => function ($attribute, $value, $fail) {
                try {
                    $client = new Client([
                        'base_uri' => env('API_SUBSCRIPTION_URL') . '/api/subscription/'
                    ]);
                    $client->request('GET', 'package-group/' . $value);
                } catch (\Exception $e) {
                    $fail($attribute.' is invalid.');
                }
            },
            'start_at' => '',
            'roles' => function ($attribute, $value, $fail) {
                $client = new Client([
                    'base_uri' => env('API_SUBSCRIPTION_URL') . '/api/subscription/'
                ]);
                $response = $client->request('GET', 'package-group/' . request()->group_id);
                $response = (array) json_decode($response->getBody()->getContents());
                foreach ($response['data']->prices as $price) {
                    $result[] = $price->pivot->package_role_id;
                }
                $request = array_column($value, 'package_role_id');
                if (array_diff($request, $result)) {
                    $fail($attribute.' is invalid.');
                }
            }
        ];
    }
}
