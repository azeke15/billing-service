<?php

namespace App\Http\Requests;

use App\Statuses\Order\OrderPaidStatus;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductsInOrder extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'products' => 'required|array',
            'products.*' => [
                'exists:products,id',
            ],
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $order = $this->route('order');

            if($order->statuses()->orderBy('id','desc')->first()->status_id  >= (new OrderPaidStatus())->getStatus())
            {
                $validator->errors()->add('order', trans('validation.orders.order-paid'));
            }
        });
    }
}
