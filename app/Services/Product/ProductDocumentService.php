<?php namespace App\Services\Product;

use App\Accounts\Balance;
use App\AccountTransaction;
use App\Order;
use App\Product;
use Carbon\Carbon;
use GuzzleHttp\Client;

class ProductDocumentService extends ProductService
{
    protected $order;
    protected $cost;
    protected $title = 'Документ';
    protected $data = [];

    public const type = 'document';

    public function __construct(Product $product = null)
    {
        parent::__construct($product);
        $this->cost = $product['cost'];
        $this->order = $product['order'];
    }

    public function process()
    {
        return 'Документ успешно куплен';
    }

    public function refund()
    {
        return 'Coming soon';
    }

    public function getAmount(): float
    {
        return $this->cost;
    }

    public function fillData(array $data)
    {
        $this->order    = null;
        $this->cost     = $data['cost'];
        $this->title    = $data['title'] ?? $this->title;
        $this->data     = $data['data'];
    }

    public function storeProduct()
    {
        if (!$this->product)
            $this->product = Product::create([
                'cost'      => $this->getAmount(),
                'title'     => $this->title,
                'type'      => static::type,
                'data'      => $this->data
            ]);

        return $this->product;
    }

    protected function getOrder(): Order
    {
        return $this->order ?? $this->product->order;
    }
}
