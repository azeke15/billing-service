<?php namespace App\Services\Product;

use App\Contracts\ProductContract;
use App\Order;
use App\Product;

abstract class ProductService implements ProductContract
{

    public const type = null;
    protected $product;

    public function __construct(Product $product = null)
    {
        $this->product = $product;
    }

    protected abstract function getOrder(): Order;
}
