<?php namespace App\Services\Product;

use App\Accounts\Balance;
use App\AccountTransaction;
use App\Order;
use App\Product;

class ProductBalanceService extends ProductService
{
    protected $order;
    protected $cost;
    protected $title = 'Пополнение баланса';
    protected $data = [];

    public const type = 'balance';

    public function __construct(Product $product = null)
    {
        parent::__construct($product);
        $this->cost = $product['cost'];
        $this->order = $product['order'];
    }

    public function process()
    {
        $account = Balance::forUser($this->getOrder()->user_id);

        $transaction = new AccountTransaction([
            'amount'    => $this->getAmount(),
            'type'      => static::type,
            'comment'   => $this->title
        ]);

        $transaction->account()->associate($account);
        $transaction->save();

        $account->applyTransaction($transaction);
    }

    public function refund()
    {
        return 'Coming soon';
        /*$account = Balance::forUser($this->getOrder()->user_id);

        $transaction = new AccountTransaction([
            'amount'    => -$this->getAmount(),
            'type'      => static::type,
            'comment'   => 'Возврат средств'
        ]);

        $transaction->account()->associate($account);
        $transaction->save();

        $account->applyTransaction($transaction);*/
    }

    public function getAmount(): float
    {
        return $this->cost;
    }

    public function fillData(array $data)
    {
        $this->order    = null;
        if (isset($data['cost'])) {
            $this->cost = (float) $data['cost'];
        } elseif (isset($data['sum'])) {
            $this->cost = (float) $data['sum'];
        }
        $this->title    = $data['title'] ?? $this->title;
        $this->data     = $data['data'] ?? $this->data;
    }

    public function storeProduct()
    {
        if (!$this->product)
            $this->product = Product::create([
                'cost'      => $this->getAmount(),
                'title'     => $this->title,
                'type'      => static::type,
                'data'      => $this->data
            ]);
        return $this->product;
    }

    protected function getOrder(): Order
    {
        return $this->order ?? $this->product->order;
    }
}
