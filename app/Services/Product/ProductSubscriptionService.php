<?php namespace App\Services\Product;

use App\Accounts\Balance;
use App\AccountTransaction;
use App\Order;
use App\Product;
use Carbon\Carbon;
use GuzzleHttp\Client;

class ProductSubscriptionService extends ProductService
{
    protected $order;
    protected $cost;
    protected $title = 'Подписка';
    protected $data = [];

    public const type = 'subscription';

    public function __construct(Product $product = null)
    {
        parent::__construct($product);
        $this->cost = $product['cost'];
        $this->order = $product['order'];
    }

    public function process()
    {
        //BUYING SUBSCRIPTION
        $client = new Client([
            'base_uri' => env('API_SUBSCRIPTION_URL') . '/api/subscription/'
        ]);

        $data = $this->product->data;
        $data['date'] = $data['date'] ?? Carbon::now()->toDateTimeString();
        $response = $client->request('POST', 'package-group/' . $this->product->data['group_id'] . '/buy', [
            'json' => $data,
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
        //END BUYING

        //GET SUBSCRIPTION ID
        $response = $this->toArray($response->getBody()->getContents());
        $this->product->data = array_merge($this->product->data, ['subscription_id' => $response->data->id]);
        $this->product->save();
        //END GET SUBSCRIPTION ID
    }

    public function refund()
    {
        $client = new Client([
            'base_uri' => env('API_SUBSCRIPTION_URL') . '/api/subscription/'
        ]);

        $response = $client->request('GET', 'subscription/' . $this->product->data['subscription_id']);
        $response = $this->toArray($response->getBody()->getContents());
        $cost = $this->product->cost;
        if (strtotime($response->data->info->ended_at) <= strtotime(Carbon::now()->toDateTimeString())) {
            throw new \Exception('subscription date has expired');
        }

        $max_sec = strtotime($response->data->info->ended_at) - strtotime($response->data->info->started_at);
        $left_sec = strtotime($response->data->info->ended_at) - strtotime(Carbon::now()->toDateTimeString());

        if ($max_sec < $left_sec) {
            $refund_cost = $cost - 500;
        } else {
            $refund_cost = (int) (($left_sec * $cost) / $max_sec);
            $refund_cost = intdiv($refund_cost, 500) * 500;
        }

        $account = Balance::forUser($this->getOrder()->user_id);
        $account->applyTransaction(new AccountTransaction([
            'amount' => $refund_cost,
            'comment' => 'Возврат средств в баланс'
        ]));

        $client->request('POST', 'subscription/' . $this->product->data['subscription_id'] . '/refund');

    }

    public function getAmount(): float
    {
        return $this->cost;
    }

    public function fillData(array $data)
    {
        $this->order    = null;
        $this->cost     = $this->getSubscriptionCost(request()->roles);
        $this->title    = $data['title'] ?? $this->title;
        $this->data     = [
            'subscription_id' => null,
            'group_id'        => $data['group_id'],
            'date'            => $data['start_at'] ?? null,
            'roles'           => $data['roles']
        ];
    }

    private function getSubscriptionCost($roles) {
        $roles_uri = '';
        foreach ($roles as $role) {
            $roles_uri = $roles_uri . '&roles[]=' .$role['package_role_id'];
        }
        $client = new Client([
            'base_uri' => env('API_SUBSCRIPTION_URL') . '/api/subscription/'
        ]);
        $response = $client->request('GET', 'package-group/' . request()->group_id . '?' . $roles_uri);
        $json = $this->toArray($response->getBody());

        return $json->data->total_price;
    }

    private function toArray($json) {
        return json_decode($json);
    }

    public function storeProduct()
    {
        if (!$this->product)
            $this->product = Product::create([
                'cost'      => $this->getAmount(),
                'title'     => $this->title,
                'type'      => static::type,
                'data'      => $this->data
            ]);

        return $this->product;
    }

    protected function getOrder(): Order
    {
        return $this->order ?? $this->product->order;
    }
}
