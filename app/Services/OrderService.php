<?php namespace App\Services;

use App\Events\Order\OrderUpdateProductEvent;
use App\Order;
use App\Product;
use Illuminate\Support\Collection;

class OrderService
{

    public function create(array $data, $products = null)
    {
        $order = new Order($data);
        $order->save();
        if (!empty($products)) {
            $this->associateProducts($order, $products);
        }

        return $order;
    }

    public function associateProducts(Order $order, $products)
    {
        if (is_array($products)) {
            $products = Product::whereIn('id', $products)->get();
        }
        if ($products instanceof Product) {
            $products = collect([$products]);
        }
        if ($products instanceof Collection) {
            $order->products()->whereNotIn('id', $products->pluck('id'))->delete();
            foreach ($products as $product) {
                $product->order()->associate($order);
                $product->save();
            }
            event(new OrderUpdateProductEvent($order));
        }
    }

}