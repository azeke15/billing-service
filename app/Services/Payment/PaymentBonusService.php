<?php namespace App\Services\Payment;

use App\Accounts\Bonus;
use App\AccountTransaction;
use App\Order;
use App\Statuses\Order\OrderTriedStatus;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PaymentBonusService extends PaymentService
{

    protected $amount;

    public function process()
    {
        //списать баланс
        $this->getOrder()->setStatus(new OrderTriedStatus());

        $account = Bonus::forUser($this->getOrder()->user_id);

        $transaction = new AccountTransaction([
            'amount'    => -$this->getPaymentAmount(),
            'type'      => static::type,
            'comment'   => 'Оплата с бонуса'
        ]);

        $transaction->account()->associate($account);
        $transaction->save();

        $account->applyTransaction($transaction);

        $status = $this->confirmOrder($this->getOrder());
        $this->storePaidHistory($status);

        $this->runProductsAction($this->getOrder());

        return response('ok');
    }

    public function getPaymentAmount(): float
    {
        return $this->getOrder()->cost;
    }

    public function fillData(Request $request)
    {
        $this->order = Order::find($request->get('order_id'));
    }

    protected function getOrder(): Order
    {
        return $this->order;
    }
}
