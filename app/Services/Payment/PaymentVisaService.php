<?php namespace App\Services\Payment;

use App\Order;
use Illuminate\Http\Request;

class PaymentVisaService extends PaymentService
{

    protected $amount;

    public function process()
    {
        return 'visa success';
    }

    public function getPaymentAmount(): float
    {
        return $this->amount;
    }

    public function fillData(Request $request)
    {
        $this->order = Order::find($request->get('account'));
        $this->amount = (float)$request->get('amount');
    }

    protected function getOrder(): Order
    {
        return $this->order;
    }
}