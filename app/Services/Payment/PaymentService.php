<?php

namespace App\Services\Payment;

use App\Contracts\PaymentContract;
use App\Contracts\ProductContract;
use App\Events\Order\OrderPaidEvent;
use App\Order;
use App\OrderStatus;
use App\PaymentHistory;
use App\Services\OrderService;
use App\Services\Product\ProductBalanceService;
use App\Statuses\Order\OrderPaidStatus;
use Dogovor24\Queue\Jobs\Billing\PaymentSuccessEmailJob;
use Dogovor24\Queue\Jobs\User\UserRegisterJob;

abstract class PaymentService implements PaymentContract
{
    protected $order;
    protected abstract function getOrder(): Order;

    public function checkStatus()
    {
        $order = $this->getOrder();

        if ($order->statuses()->orderBy('id', 'desc')->first()->status_id >= (new OrderPaidStatus())->getStatus()) {
            return false;
        }
        return true;
    }

    public function checkAmount()
    {
        if (!$this->checkStatus()) return false;

        if ($this->order->cost != $this->getPaymentAmount()) {
            $balance = new ProductBalanceService();
            $cost = $this->getPaymentAmount();
            if ($this->order->cost < $this->getPaymentAmount()) {
                $cost = $this->getPaymentAmount() - $this->order->cost;
            } else {
                $orderService = new OrderService();
                $this->order = $orderService->create(['user_id' => $this->order->user_id]);
            }
            $balance->fillData([
                'cost' => $cost,
            ]);
            $product = $balance->storeProduct();
            $product->order()->associate($this->order);
            $product->save();
        }
    }

    public final function confirmOrder(Order $order)
    {
        $status = $order->setStatus(new OrderPaidStatus());

        event(new OrderPaidEvent($order));

        $payment = $this->parseClassName();
        $products = $this->getOrderInfo();
        PaymentSuccessEmailJob::dispatch($this->order->id, $this->order->user_id, $products, $payment);

        return $status;
    }

    protected final function runProductsAction($order)
    {
        $order->load('products');
        foreach ($order->products as $product) {
            /* @var $productService ProductContract */
            $productService = app()->makeWith(ProductContract::class, [$product]);
            $productService->process();
        }
    }

    public function storeHistory(OrderStatus $status, $data = [])
    {
        $order = $this->getOrder();

        $class_name = $this->parseClassName();

        $transaction = new PaymentHistory(['type' => $class_name, 'amount' => $this->getPaymentAmount(), 'data' => $data]);
        $transaction->statuses()->associate($status);
        $transaction->order()->associate($order);
        $transaction->save();
        return $transaction;

    }

    public function storePaidHistory(OrderStatus $status, $data = [])
    {
        $order = $this->getOrder();

        $payment = $this->storeHistory($status, $data);

        $order->success_payment_id = $payment->id;
        $order->save();
    }

    public function storeFailedHistory(OrderStatus $status, $data = [])
    {
        $this->storeHistory($status->id, $data);
    }

    private function parseClassName() {
        return kebab_case(preg_replace('/.*Payment(.+)Service$/', '$1', static::class));
    }

    private function getOrderInfo() {
        $products = $this->order->products;
        foreach ($products as $product) {
            $list[] = [
                'order_id' => $product->order_id,
                'cost'     => $product->cost,
                'title'    => $product->title,
                'type'     => $product->type,
                'data'     => $product->data
            ];
        }
        $products = [
            'order_cost' => $this->getOrder()->cost,
            'products' => $list
        ];

        return $products;
    }
}
