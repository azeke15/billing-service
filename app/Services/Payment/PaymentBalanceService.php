<?php namespace App\Services\Payment;

use App\Account;
use App\Accounts\Balance;
use App\AccountTransaction;
use App\Order;
use App\Product;
use App\Services\Product\ProductBalanceService;
use App\Services\Product\ProductSubscriptionService;
use App\Statuses\Order\OrderFailedStatus;
use App\Statuses\Order\OrderTriedStatus;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PaymentBalanceService extends PaymentService
{

    protected $amount;

    public function process()
    {
        //списать баланс
        $this->getOrder()->setStatus(new OrderTriedStatus());

        $account = Balance::forUser($this->getOrder()->user_id);

        $transaction = new AccountTransaction([
            'amount'    => -$this->getPaymentAmount(),
            'type'      => ProductBalanceService::type,
            'comment'   => 'Оплата с баланса'
        ]);

        $transaction->account()->associate($account);
        $transaction->save();

        $account->applyTransaction($transaction);

        $status = $this->confirmOrder($this->getOrder());
        $this->storePaidHistory($status);

        $this->runProductsAction($this->getOrder());

        return response('ok');
    }

    public function getPaymentAmount(): float
    {
        return $this->getOrder()->cost;
    }

    public function fillData(Request $request)
    {
        $this->order = Order::find($request->get('order_id'));
    }

    protected function getOrder(): Order
    {
        return $this->order;
    }
}
