<?php namespace App\Services\Payment;

use App\Order;
use App\Statuses\Order\OrderFailedStatus;
use App\Statuses\Order\OrderTriedStatus;
use Illuminate\Http\Request;

class PaymentQiwiService extends PaymentService
{

    protected $amount;

    public function process()
    {
        $status = $this->getOrder()->setStatus(new OrderTriedStatus());

        $response = $this->sendRequest();

        $this->storeHistory($status, $response['data']);

        if($response['status'])
        {
            $this->runProductsAction($this->getOrder());
            $status = $this->confirmOrder($this->getOrder());
            $this->storePaidHistory($status, $response['data']);
            return 'qiwi success';
        }else{

            $status = $this->getOrder()->setStatus(new OrderFailedStatus());
            $this->storeFailedHistory($status, $response['data']);

            return 'qiwi failed';
        }
    }

    public function getPaymentAmount(): float
    {
        return $this->amount;
    }

    public function fillData(Request $request)
    {
        $this->order = Order::find($request->get('invoice'));
        $this->amount = (float)$request->get('sum');
    }

    protected function getOrder(): Order
    {
        return $this->order;
    }

    private function sendRequest()
    {
        return ['status' => true, 'data' => ['provider'=>'qiwi']];
    }
}
