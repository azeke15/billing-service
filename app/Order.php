<?php

namespace App;

use App\Events\Order\OrderCreatedEvent;
use App\Statuses\Order\OrderCreatedStatus;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $fillable = [
        'user_id',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'cost' => 'float',
    ];

    protected $appends = [
        'cost'
    ];

    public function statuses()
    {
        return $this->hasMany(OrderStatus::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function histories()
    {
        return $this->hasMany(PaymentHistory::class, 'order_id');
    }

    public function paymentHistory()
    {
        return $this->belongsTo(PaymentHistory::class, 'success_payment_id');
    }

    public function getCostAttribute()
    {
        return $this->products->sum('cost');
    }

    public function setStatus(\App\Statuses\OrderStatus $orderStatus)
    {
        $status = new OrderStatus();
        $status->order()->associate($this);
        $status->status_id = $orderStatus->getStatus();
        $status->save();
        return $status;
    }

    protected static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $model->setStatus(new OrderCreatedStatus());
            event(new OrderCreatedEvent($model));
        });
    }
}
