<?php

namespace App\Listeners;

use App\Services\OrderService;
use App\Services\Product\ProductDocumentService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class CreateProductOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $product = new ProductDocumentService();
        $product->fillData([
            'cost' => (float) 0,
            'data' => [
                'document_id' => $event->product_id
            ]
        ]);
        $product = $product->storeProduct();

        (new OrderService())->create([
            'user_id' =>  $event->user_id
        ], [$product->id]);
    }
}
