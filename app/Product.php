<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'cost',
        'title',
        'type',
        'data',
    ];

    protected $casts = [
        'order_id' => 'integer',
        'cost' => 'float',
        'data' => 'array',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
