<?php

namespace App\Contracts;

use App\AccountTransaction;

interface AccountTransactionableContract
{

    public function applyTransaction(AccountTransaction $transaction);
}
