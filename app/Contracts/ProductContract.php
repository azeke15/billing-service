<?php

namespace App\Contracts;

interface ProductContract
{
    public function fillData(array $data);
    public function process();
    public function refund();
    public function storeProduct();
    public function getAmount(): float;
}
