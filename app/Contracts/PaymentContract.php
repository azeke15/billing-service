<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface PaymentContract
{
    public function fillData(Request $request);
    public function process();
    public function checkAmount();
    public function getPaymentAmount(): float;
}
