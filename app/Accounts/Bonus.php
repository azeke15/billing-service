<?php

namespace App\Accounts;

use App\Account;
use App\AccountTransaction;
use App\Contracts\AccountTransactionableContract;

class Bonus extends Account implements AccountTransactionableContract
{

    public function applyTransaction(AccountTransaction $transaction)
    {
        $this->balance += $transaction->amount;
        $this->save();

        return parent::applyTransaction($transaction);
    }
}
