<?php

namespace App;

use App\Contracts\AccountTransactionableContract;
use App\Events\AccountCreatedEvent;
use App\Events\AccountTransactionEvent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Account extends Model implements AccountTransactionableContract
{

    protected $table = 'accounts';

    protected $fillable = [
        'user_id',
        'balance',
        'type',
        'title',
        'description',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'balance' => 'float',
    ];

    public function transactions()
    {
        return $this->hasMany(AccountTransaction::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type', function (Builder $builder) {
            if(static::class !== self::class)
                $builder->where('type', kebab_case(class_basename(static::class)));
        });

        self::created(function($model){
            event(new AccountCreatedEvent($model));
        });
    }

    public function applyTransaction(AccountTransaction $transaction)
    {
        event(new AccountTransactionEvent($transaction));
    }

    public static function forUser($userId)
    {
        return static::where('user_id', $userId)->first();
    }
}
