<?php

namespace App\Providers;

use App\Account;
use App\Contracts\AccountTransactionableContract;
use App\Contracts\PaymentContract;
use App\Contracts\PaymentFormRequestContract;
use App\Contracts\ProductContract;
use App\Contracts\ProductFormRequestContract;
use App\Product;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(AccountTransactionableContract::class, function ($app, $params) {
            if ($params) {

                $class = config('billing.accounts_namespace').studly_case($params['type']);
                $account = $class::where([
                    'user_id' => $params['order']['user_id'],
                    'type'    => $params['type'],
                ])->first();

                if ($account) {
                    return $account;
                }
                abort(404);
            } else {

                $account = Account::findOrFail(request('account_id'));
                $class = config('billing.accounts_namespace').studly_case($account->type);

                return $class::findOrFail($account->id);
            }
        });

        $this->app->bind(PaymentFormRequestContract::class, function () {
            $payment = config('billing.payment_form_requests_namespace').'StorePayment'.studly_case(request('type'));
            if (class_exists($payment)) {
                return $this->app->make($payment);
            }
            abort(404);
        });

        $this->app->bind(ProductFormRequestContract::class, function () {
            $payment = config('billing.product_form_requests_namespace').'StoreProduct'.studly_case(request('type'));
            if (class_exists($payment)) {
                return $this->app->make($payment);
            }
            abort(404);
        });

        $this->app->bind(PaymentContract::class, function () {
            $payment = config('billing.payment_services_namespace').'Payment'.studly_case(request('type')).'Service';
            if (class_exists($payment)) {
                return new $payment();
            }
            abort(404);
        });

        $this->app->bind(ProductContract::class, function ($app, $params) {

            $product = null;
            if (isset($params[0]) && ($product = $params[0]) instanceof Product) {
                $productService = config('billing.product_services_namespace').'Product'.studly_case($params[0]->type).'Service';
            } else {
                $productService = config('billing.product_services_namespace').'Product'.studly_case(request('type')).'Service';
            }
            if (class_exists($productService)) {
                return new $productService($product);
            }

            abort(404);
        });
    }
}
