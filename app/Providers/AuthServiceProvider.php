<?php

namespace App\Providers;

use App\Account;
use App\AccountTransaction;
use App\Order;
use App\PaymentHistory;
use App\Policies\AccountPolicy;
use App\Policies\OrderActionsPolicy;
use App\Policies\OrderPolicy;
use App\Policies\PaymentHistoryPolicy;
use App\Policies\TransactionPolicy;
use App\Product;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Account::class            => AccountPolicy::class,
        AccountTransaction::class => TransactionPolicy::class,
        PaymentHistory::class     => PaymentHistoryPolicy::class,
        Order::class              => OrderPolicy::class,
        Product::class            => OrderActionsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
    }
}
