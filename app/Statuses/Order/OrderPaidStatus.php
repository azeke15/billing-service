<?php

namespace App\Statuses\Order;

use App\Statuses\OrderStatus;

class OrderPaidStatus extends OrderStatus
{
    public function __construct()
    {
        $this->setStatus(900);
    }
}
