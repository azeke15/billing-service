<?php

namespace App\Statuses\Order;

use App\Statuses\OrderStatus;

class OrderFailedStatus extends OrderStatus
{
    public function __construct()
    {
        $this->setStatus(700);
    }
}
