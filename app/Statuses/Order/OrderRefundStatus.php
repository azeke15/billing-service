<?php

namespace App\Statuses\Order;

use App\Statuses\OrderStatus;

class OrderRefundStatus extends OrderStatus
{
    public function __construct()
    {
        $this->setStatus(1100);
    }
}
