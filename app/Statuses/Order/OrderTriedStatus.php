<?php

namespace App\Statuses\Order;

use App\Statuses\OrderStatus;

class OrderTriedStatus extends OrderStatus
{
    public function __construct()
    {
        $this->setStatus(600);
    }
}
