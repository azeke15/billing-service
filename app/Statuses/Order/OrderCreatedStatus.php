<?php

namespace App\Statuses\Order;

use App\Statuses\OrderStatus;

class OrderCreatedStatus extends OrderStatus
{
    public function __construct()
    {
        $this->setStatus(100);
    }
}
