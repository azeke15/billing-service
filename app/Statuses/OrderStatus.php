<?php

namespace App\Statuses;


abstract class OrderStatus
{
    private $status;
    private $description;

    public function getStatus()
    {
        return $this->status;
    }

    protected function setStatus($value)
    {
        $this->status = $value;
    }
}
