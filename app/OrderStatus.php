<?php

namespace App;

use App\Events\Order\OrderStatusChangedEvent;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{

    protected $table = 'order_statuses';

    protected $fillable = [
        'order_id',
        'status_id',
    ];

    protected $casts = [
        'order_id' => 'integer',
        'status_id' => 'integer',
    ];

    const UPDATED_AT = null;

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    protected static function boot()
    {
        parent::boot();

        self::created(function($model){
            event(new OrderStatusChangedEvent($model));
        });
    }
}
