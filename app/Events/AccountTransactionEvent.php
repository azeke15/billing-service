<?php

namespace App\Events;

use App\AccountTransaction;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AccountTransactionEvent
{

    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $transaction;

    public function __construct(AccountTransaction $transaction)
    {
        $this->transaction = $transaction;
    }
}
