<?php

namespace App\Events;

use App\Account;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AccountCreatedEvent
{

    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $account;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }
}
