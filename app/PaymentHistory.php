<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{

    protected $table = 'payment_histories';

    protected $fillable = [
        'type',
        'amount',
        'data',
    ];

    protected $casts = [
        'amount' => 'float',
        'data'   => 'array',
    ];

    const UPDATED_AT = null;

    public function statuses()
    {
        return $this->belongsTo(OrderStatus::class,'order_status_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
