<?php

namespace App\Policies;

use App\Order;
use App\User;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{

    use HandlesAuthorization;

    public function view(?User $user, Order $order)
    {
        return $order->user_id == (new AuthUserService())->getId() or (new AuthAbilityService())->userHasAbility('billing-order-view');
    }

    public function update(?User $user, Order $order)
    {
        return $order->user_id == (new AuthUserService())->getId() or (new AuthAbilityService())->userHasAbility('billing-order-update');
    }

}
