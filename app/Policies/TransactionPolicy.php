<?php

namespace App\Policies;

use App\AccountTransaction;
use App\User;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{

    use HandlesAuthorization;

    public function view(?User $user, AccountTransaction $accountTransaction)
    {
        return $accountTransaction->account->user_id == (new AuthUserService())->getId() or (new AuthAbilityService())->userHasAbility('billing-account-transaction-view');
    }

}
