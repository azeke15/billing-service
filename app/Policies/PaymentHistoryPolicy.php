<?php

namespace App\Policies;

use App\PaymentHistory;
use App\User;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentHistoryPolicy
{

    use HandlesAuthorization;

    public function view(?User $user, PaymentHistory $paymentHistory)
    {
        return $paymentHistory->order->user_id == (new AuthUserService())->getId() or (new AuthAbilityService())->userHasAbility('billing-payment-history-view');
    }

}
