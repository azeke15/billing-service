<?php

namespace App\Policies;

use App\Account;
use App\User;
use Dogovor24\Authorization\Services\AuthAbilityService;
use Dogovor24\Authorization\Services\AuthUserService;
use Illuminate\Auth\Access\HandlesAuthorization;

class AccountPolicy
{

    use HandlesAuthorization;

    public function view(?User $user, Account $account)
    {
        return $account->user_id == (new AuthUserService())->getId() or (new AuthAbilityService())->userHasAbility('billing-account-view');
    }

    public function update(?User $user, Account $account)
    {
        return $account->user_id == (new AuthUserService())->getId() or (new AuthAbilityService())->userHasAbility('billing-account-update');
    }
}
