<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Для пользователей
Route::group(['prefix' => 'billing',  'middleware' => 'auth.api'], function () {
    Route::apiResource('account', 'AccountController')->except(['destroy', 'store']);
    Route::apiResource('order', 'OrderController')->only(['index', 'show']);
    Route::apiResource('transaction', 'TransactionController')->only(['index', 'show']);
    Route::apiResource('payment-history', 'PaymentHistoryController')->only(['index', 'show']);
    Route::post('order-product/{type}', 'OrderProductRequestController@store');
    Route::apiResource('order-refund', 'RefundController')->parameters([
        'order-refund' => 'order'
    ])->only(['update']);
    Route::apiResource('order-products', 'OrderProductsController')->parameters([
        'order-products' => 'order'
    ])->only(['update']);
});

Route::group(['prefix' => 'billing'], function () {
    Route::any('payment/{type}', 'PaymentController@store');
    Route::get('is-product-paid/{type}/{product_id}', 'IsProductPaidController@show');
});
