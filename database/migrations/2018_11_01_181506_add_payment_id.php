<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint  $table){
            $table->unsignedInteger('success_payment_id')->nullable()->default(null);
            $table->foreign('success_payment_id')->references('id')->on('payment_histories')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint  $table){
            $table->dropForeign(['success_payment_id']);
            $table->dropColumn('success_payment_id');
        });
    }
}
