<?php

return [

    'accounts_namespace' => '\\App\\Accounts\\',
    'payment_form_requests_namespace' => '\\App\\Http\\Requests\\Payment\\',
    'product_form_requests_namespace' => '\\App\\Http\\Requests\\Product\\',
    'payment_services_namespace' => '\\App\\Services\\Payment\\',
    'product_services_namespace' => '\\App\\Services\\Product\\',
];
